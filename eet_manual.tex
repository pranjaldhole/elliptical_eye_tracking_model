\documentclass[12pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{tabularx}
\usepackage{braket}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[left=0.5cm,right=0.5cm,top=2cm,bottom=2cm]{geometry}
\usepackage{float}
\pagestyle{headings}
\usepackage{hyperref}
\usepackage{pifont}
\usepackage[onehalfspacing]{setspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\1}{\left(\phi \right)}
\newcommand{\2}{\left(\theta \right)}
\newcommand{\3}{\left( \psi \right)}
\newcommand{\s}{\sin}
\newcommand{\az}{\cos}
\newcommand{\4}{\left(\omega\right)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Elliptical pupil eye-tracking}
\subsection{Outline}
\par In this project we extend the weak perspective model for the eye-tracking previously used in \cite{ratmodel} in order to accommodate for the elliptical pupil in the eye-tracking software\footnote{For detailed account of the methodology, please refer to [Supplementary methods, \textit{in preparation}].}.

% Given that a point $X$ lying on a conic section $\mathcal{C}$ that describes an ellipse in three dimensions
 \par \textbf{Goal:} We need to determine the physical parameters of the eyeball (also termed as global parameters), i.e., the ratio between the minor and major axes ($\alpha$), the maximum pupil rotation radius in pixels ($\frac{f}{z_0}$) and the eyeball center($a_{EC}, b_{EC}$) that determines the physical state of the eye. 
 
 \par This will allow us to to exactly track the pupil boundary for any given dilation/contraction radius ($r$) and the three rotations of the eyeball: pitch($\theta$) - rotation in vertical axis, roll($\psi$) - rotation around optical axis (ocular torsion), yaw($\phi$) - rotation in horizontal axis.
  
 \par We use weak perspective model to relate the shape of the pupil in 3-dimensions to its 2-dimensional image taken by the eye-camera.
 
 \par \textbf{Theorem}: \emph{A given point in 3-dimensions $X$ and its image $\overline{X}$ is related by homography up to a scale factor.}
 \begin{align*}
 HX = \lambda \overline{X}
 \end{align*}
 where $\lambda$ is an arbitrary scale factor.
 \par A homography has 8 projective degrees of freedom \footnote{Please refer to \cite{multiviewgeometry} for details on projective geometry.}.
\subsection{Rotation Matrix}
The rotation matrices are given by:

\begin{align}
R_{\phi} = \begin{pmatrix}
 1 & 0 & 0 \\
 0 & \cos (\phi) &  \sin (\phi) \\
 0 &  -\sin (\phi) & \cos (\phi)
\end{pmatrix}
\end{align}

\begin{align}
R_{\theta} = \begin{pmatrix}
 \cos (\theta) & 0 & -\sin (\theta) \\
 0 & 1 & 0 \\
 \sin (\theta) & 0 & \cos (\theta)
\end{pmatrix}
\end{align}

\begin{align}
R_{\psi} = \begin{pmatrix}
\cos (\psi) & \sin (\psi) & 0 \\
-\sin (\psi) & \cos (\psi) & 0 \\
0 & 0 & 1
\end{pmatrix}
\end{align}

The resultant rotation matrix can be given as:
$$ R = R_{\phi}R_{\theta}R_{\psi} $$
\begin{align}
R = \begin{pmatrix}
\cos (\theta) \cos (\psi) & \cos (\theta) \sin \3 & -\sin \2 \\
 \s \2 \s \1 \az \3 - \az \1 \s \3 &  \s \2 \s \1 \s \3 + \az \1 \az \3 & \s \1 \az \2 \\
\az \1 \s \2 \az \3 + \s \1 \s \3 & \az \1 \s \2 \s \3 - \az \3 \s \1 & \az \1 \az \2
\end{pmatrix}
\end{align}

Homography or projective transformation maps a point in a world coordinates to point in camera coordinate system (\textit{image}).

The homography matrix is given by

\begin{align}
H = PR^*Q = \begin{pmatrix}
H_{11} & H_{12} & H_{13} \\
H_{21} & H_{22} & H_{23} \\
H_{31} & H_{32} & H_{33}
\end{pmatrix}
\end{align}
where $P, R^* \text{ and } Q$ are Projection matrix, $4 \times 4$ Rotation matrix and Embedding matrix respectively given as

\begin{align}
P = \begin{pmatrix}
\frac{f}{z_0} & 0 & 0 & a_{EC} \\
0 & \frac{f}{z_0} & 0 & b_{EC} \\
0 & 0 & 0 & 1
\end{pmatrix}
\end{align}

\begin{align}
R^* = \begin{pmatrix}
R & \mathbf{0} \\
\mathbf{0} & 1
\end{pmatrix}
\end{align}

\begin{align}
Q = \begin{pmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & -1 \\
0 & 0 & 1
\end{pmatrix}
\end{align}

\par A general definition of conic is given in matrix form as follows:

\begin{align}
\begin{array}{rcl}
\begin{pmatrix}
x & y & 1
\end{pmatrix} \begin{pmatrix}
A & B/2 & D/2 \\
B/2 & C & E/2 \\
D/2 & E/2 & F
\end{pmatrix} \begin{pmatrix}
x \\
y \\
1
\end{pmatrix} &=& 0 \\
X^T C X &=& 0
\end{array}
\label{ellipse}
\end{align}

For an ellipse, the algebraic coefficients can be re-expressed in terms of trigonometric parameters of the ellipse as:
\begin{align}
\begin{array}{rcl}
A &=& L^2 \s^2 \4 + l^2 \az^2 \4 \\
B &=& 2 ( l^2 - L^2) \s \4 \az \4 \\
C &=& L^2 \az^2 \4 + l^2 \s^2 \4 \\
D &=& -2A a_{PC} - B b_{PC} \\
E &=& -2C b_{PC} - B a_{PC} \\
F &=& A a_{PC}^2 + B a_{PC} b_{PC} +  C b_{PC}^2 - L^2 l^2
\end{array}
\end{align}
where $L, l, \omega, (a_{PC}, b_{PC})$ are length of major and minor axes, inclination angle of the ellipse and center of the ellipse (PC denotes the pupil center when shape of pupil is assumed to be an ellipse).

\par Consider a point $X = [x,y,1]^T$ that lies on the ellipse given by Eq. (\ref{ellipse}). The relation between the point $X$ and its image $X'$ is given by Homography $H$.
\begin{align}
X' = HX
\end{align}

Thus the conic transforms as follows:
\begin{align*}
\begin{array}{rcl}
X^T C X &=& (H^{-1} X')^T C (H^{-1} X') \\
 & =& X'^T (H^{-T} C H^{-1}) X' \\
 & =& X'^T C' X'
\end{array}
\end{align*}
The above transformation tells us that if a point $X$ lies on the conic $C$, then the point in image $X'$ lies on the conic $C'$.
\begin{align}
C' = H^{-T} C H^{-1} = \left( H C^{-1} H^T \right)^{-1}
\end{align}

\par Let $D$ be the ellipse obtained from real data in the matrix form. % having five degrees of freedom in the form of length of major and minor axes length, inclination angle and ellipse center.
Then we can compare the conic $C'$ to the data matrix $D$ given as follows:
\begin{align}
C' = H^{-T} C H^{-1} = \lambda D
\end{align}

where $\lambda$ is an arbitrary scale factor.

\begin{align}
C'^{-1} = H C^{-1} H^T = \frac{D^{-1}}{\lambda}
\end{align}
The homography matrix has six degrees of freedom (3 global + 3 local) whereas the conic has two degrees of freedom (1 local, dilation/contraction radius + 1 global, eccentricity). Thus overall we have 8 degrees of freedom (4 global + 4 local) in the model.

\par If we are given $n$ images, then the number of parameters available in the model are $4n$ local + 4 global. In the data, we have $5n$ degrees of freedom.

\begin{align*}
5n \geq 4n + 4 \\
\therefore n \geq 4
\end{align*}

Thus we need more than or equal to 4 images to completely determine the parameters of the model.

\section{Code}
\par We collect several images of the pupil in a single session on which the pupil boundary is marked. For the optimization to work best, we need at least 10 or more images to process.
This eye-data is extracted using \textit{eydimportscript.m} script into matrix format that contains information about the ellipses in the trigonometric format, viz. pupil center x, pupil center y, inclination in radians, major axis, minor axis in columns.

\par This data is then converted to another matrix format using \textit{load\_Tdata.m}. The matrix Tdata is an n$\times$6 matrix in which each row contains the elements of the symmetric conic section matrix in order: [(1,1) (1,2) (1,3) (2,2) (2,3) (3,3)] which is our obtained data ellipse for each image collected using the eye camera.

\par We use the data ellipses as input in the form of Tdata to \textit{optimize\_global.m} which uses Nelder-Mead algorithm using the fminsearch function of matlab to minimize the mean sum of the square error between 5 points between the ellipse from the data and the elliptical pupil model.

\par The function \textit{optimize\_global.m} evaluates the values of the physical parameters of the pupil (termed as global parameters), viz., ratio of minor to the major axis (alpha), maximum pupil rotation radius in pixels, and the eyeball center.

\par The evaluated global parameters of the pupil can then be used to calibrate the eye so that we can mark the boundary of the pupil for the image data using the eye-tracking software developed at BBO lab, CAESAR, Bonn, Germany. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{12}

\bibitem{ratmodel}
Damian J. Wallace, David S. Greenberg, J\"urgen Sawinski, Stefanie Rulla, Giuseppe Notaro and Jason N. D. Kerr,
  \emph{Rats maintain an overhead binocular field at the expense of constant fusion},
  Nature 498, 65–69,
  doi:10.1038/nature12153,
  06 June 2013.

\bibitem{multiviewgeometry}
  Richard Hartley and Andrew Zisserman,
  \emph{Multiple View Geometry in computer vision},
  ch. 1 and 2,
  Cambridge University Press,
  1st edition,
  2000.

\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}