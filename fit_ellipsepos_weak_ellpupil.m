function [ea, roll, r, fv] = ...
    fit_ellipsepos_weak_ellpupil(ellipsecent, majax, minax, theta, alpha, fz, a_EC, b_EC, showwb, wbmsg)
%[ea, roll, r] = fit_ellipsepos_weak_ellpupil(ellipsecent, majax, minax, theta, alpha, fz, a_EC, b_EC, showwb, wbmsg)
nimages = numel(theta);
assign_defaults('showwb',nimages > 4,'wbmsg','Fitting weak perspective model to ellipses');
axswitch = minax > majax;
[majax(axswitch), minax(axswitch)] = deal(minax(axswitch), majax(axswitch));
theta(axswitch) = theta(axswitch) + pi / 2;

if showwb
    
    wb = waitbar(0,wbmsg);
    
end

ea = nan(2, nimages);
roll = nan(1, nimages);
[fv, r] = deal(nan(nimages,1));

for u = 1:nimages    
    
    if showwb && mod(u,10) == 1
        waitbar(u / nimages, wb);
    end
    
    [A, B, C] = trig2alg(majax(u), minax(u), theta(u));
    
    [ea(1,u), roll(u), ea(2,u), r(u), fv(u)] = fminsearch_four_local_parameters( ...
        alpha, fz, a_EC, b_EC, A, B, C, ellipsecent(:,u)');
    
end

if showwb
    close(wb);
end