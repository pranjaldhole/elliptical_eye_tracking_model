function [mean_sum_square_errors, locals, squared_errors] = calculate_mean_sum_square_errors(A,B,C,ellipsecent, alpha, fz, a_EC, b_EC)
% [mean_sum_square_errors, locals, squared_errors] = calculate_mean_sum_square_errors(A,B,C,ellipsecent, alpha, fz, a_EC, b_EC)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This program takes the marked pupil data in algebraic form as an input along with
% the values of global parameters and returns the mean of the sum of the
% square error between the elliptical pupil model ellipse and the marked
% pupil data. Along with that the function returns the values of local variables
% and squared error between marked pupil and its fit from the elliptical pupil model.
n = numel(A);
locals = zeros(n,4);
% model_matrices = zeros(n,6);
squared_errors = zeros(n,1);

for i = 1:n    
    A1 = A(i); B1 = B(i); C1 = C(i); c1 = ellipsecent(:,i)';
    
    [pitch, roll, yaw, r] = fminsearch_four_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1);
    
    locals(i,:) = [pitch roll yaw r];
    
    [A2,B2,C2,c2] = calculate_model_matrix(alpha, fz, a_EC, b_EC, pitch, roll, yaw, r);
    
%     model_matrices(i,:) = [model_matrix(1,1) model_matrix(1,2) model_matrix(1,3) model_matrix(2,2) model_matrix(2,3) model_matrix(3,3)];
       
    squared_errors(i) = error_between_two_ellipses(A1,B1,C1,c1,A2,B2,C2,c2);
end

mean_sum_square_errors = sum(squared_errors) / n;