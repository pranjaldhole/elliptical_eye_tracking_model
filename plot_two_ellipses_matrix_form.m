function plot_two_ellipses_matrix_form(ellipsemat1, ellipsemat2,linewidth)
% Caution: Comment lines 35, 37, 42 to use the function in a loop statement
% if one needs to write several plots in one figure.
% 
% plot_two_ellipses_matrix_form(ellipsemat1, ellipsemat2,linewidth)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function take two ellipses in the matrix format and plots them into
% the same graph.

%%%%% ellipse parameters:
[A1,B1,C1,c1] = mat2alg(ellipsemat1);
[major_axis1, minor_axis1, orientation1] = alg2trig(A1, B1, C1);

[A2,B2,C2,c2] = mat2alg(ellipsemat2);
[major_axis2, minor_axis2, orientation2] = alg2trig(A2, B2, C2);

[~,p1,p2] = ellipse_efunc(A1,B1,C1,c1,A2,B2,C2,c2);

R1 = [ cos(orientation1) -sin(orientation1); sin(orientation1) cos(orientation1) ];
R2 = [ cos(orientation2) -sin(orientation2); sin(orientation2) cos(orientation2) ];

S1 = diag([major_axis1 minor_axis1]);
S2 = diag([major_axis2 minor_axis2]);

% drawing the ellipse
ndrawpts = 1000;
theta_r         = linspace(0, 2 * pi, ndrawpts);
xypts = [cos(theta_r); sin(theta_r)];

ellipsepts1 = R1 * S1 * xypts + c1(:) * ones(1, ndrawpts);
ellipsepts2 = R2 * S2 * xypts + c2(:) * ones(1, ndrawpts);

figure;
plot(p1(:,1), p1(:,2), 'r*');
hold on;
plot(p2(:,1), p2(:,2), 'g*');
plot( ellipsepts1(1,:), ellipsepts1(2,:),'r','linewidth',linewidth );
plot( ellipsepts2(1,:), ellipsepts2(2,:),'g','linewidth',linewidth );
axis equal;
hold off;