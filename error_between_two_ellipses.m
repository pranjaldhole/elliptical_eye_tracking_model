function error = error_between_two_ellipses(A1,B1,C1,c1,A2,B2,C2,c2)
% error = error_between_two_ellipses(A1,B1,C1,c1,A2,B2,C2,c2)
% 
%%%%%%%%%% Read me %%%%%%%%%%%
% In this function, we compare two ellipses and calculate the sum of the
% squares of distances between points on two ellipses as an error function
% between the two ellipses.


%%%%%%% Ellipse 1 %%%%%%%%
%%%%% Test to check whether the given conic section is an ellipse or not:
test1 = B1 ^ 2 - A1 * C1;
switch test1
    case (test1<0),  status = '';
    case (test1==0), status = 'Parabola found';  warning( 'Parabola found : error_between_two_ellipses: Did not locate an ellipse' );
    case (test1>0),  status = 'Hyperbola found'; warning( 'Hyperbola found : error_between_two_ellipses: Did not locate an ellipse' );
end

%%%%%%%% Ellipse 2 %%%%%%%%%%%
%%%%% Test to check whether the given conic section is an ellipse or not:
test2 = B2 ^ 2 - A2 * C2;
switch test2
    case (test2<0),  status = '';
    case (test2==0), status = 'Parabola found';  warning( 'Parabola found : error_between_two_ellipses: Did not locate an ellipse' );
    case (test2>0),  status = 'Hyperbola found'; warning( 'Hyperbola found : error_between_two_ellipses: Did not locate an ellipse' );
end

%%% Error %%%
error = ellipse_efunc(A1,B1,C1,c1,A2,B2,C2,c2);