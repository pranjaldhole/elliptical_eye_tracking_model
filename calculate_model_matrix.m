function [A,B,C,c,model_matrix] = calculate_model_matrix(alpha, fz, a_EC, b_EC, angh, torsion, angv, r)
% [A,B,C,c,model_matrix] = calculate_model_matrix(alpha, fz, a_EC, b_EC, angh, torsion, angv, r)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the values of global and local parameters as an input
% and computes the ellipse in the matrix form and returns the ellipse 
% parameters in algebraic and matrix form for the elliptical pupil
% model.

R = pitchrollyaw2rotmat(angh, -angv, torsion); %note the different conventions of angle naming

Rf = [ R zeros(3,1); zeros(1,3) 1];

Q = [ 1 0 0; 0 1 0; 0 0 -1; 0 0 1]; %%% Embedding matrix %%%
P = [ fz 0 0 a_EC; 0 fz 0 b_EC; 0 0 0 1]; %%% Projection matrix %%%
H = P*Rf*Q; %%%%% Homography matrix %%%%%

%%%%%%%%%%%% Conic matrix %%%%%%%%%%%
M = [ 1 0 0; 0 1/alpha^2 0; 0 0 -r^2];
Ht = H';
%%%%%%%%%%%% Conic matrix for data %%%%%%%%%%%
model_matrix = Ht \ M / H;
[A,B,C,c] = mat2alg(model_matrix);