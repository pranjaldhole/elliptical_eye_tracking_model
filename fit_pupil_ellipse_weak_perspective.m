function [alpha_c, fz_c, a_EC_c, b_EC_c, fval] = fit_pupil_ellipse_weak_perspective(ellipsecent, theta, majax, minax, species)
%[alpha_c, fz_c, a_EC_c, b_EC_c, fval] = fit_pupil_ellipse_weak_perspective(ellipsecent, theta, majax, minax, species)
%
% This function takes the marked pupil data in trigonometric form as input.
% It uses optimize_global function to do fminsearch over global variables
% and returns the optimum values of global variables.

% ellipsecent = ellipsecent';
% maxfunevals = 1000;
global fit_pupil_ellipse_set_weak_funevalcount
fit_pupil_ellipse_set_weak_funevalcount = 0;
% circtol = 0.01;
nimages = numel(majax);
axswitch = minax > majax;
[majax(axswitch), minax(axswitch)] = deal(minax(axswitch), majax(axswitch));
theta(axswitch) = theta(axswitch) + pi / 2;
assert(numel(minax) == nimages && numel(theta) == nimages && all(size(ellipsecent) == [2 nimages]),'incorrect inputs');
minax = reshape(minax,nimages,1); majax = reshape(majax,nimages,1); theta = reshape(theta,nimages,1);
assert(nimages > 3, 'at least 4 images are required');

A = zeros(nimages,1);
B = zeros(nimages,1);
C = zeros(nimages,1);

for i = 1:nimages
    [A(i),B(i),C(i)] = trig2alg(majax(i), minax(i), theta(i));
end
maxiter = 1000;
[alpha_c, fz_c, a_EC_c, b_EC_c, fval] = optimize_global(A,B,C,ellipsecent, maxiter, species);
% [pitch, roll, yaw, r, majax_proj,minax_proj,theta_proj,cproj] = fit_ellipse_pos_weak(ellipsecent, majax, minax, theta, alpha_c, fz_c, a_EC_c, b_EC_c);
clear global fit_pupil_ellipse_set_weak_funevalcount;