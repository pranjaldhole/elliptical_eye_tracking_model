function error = error_between_model_and_data(alpha, fz, a_EC, b_EC, pitch, roll, yaw, r, A1,B1,C1,c1)
% error = error_between_model_and_data(alpha, fz, a_EC, b_EC, pitch, roll, yaw, r, A1,B1,C1,c1)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes values of global and local parameters as input
% computes the ellipse for the elliptical pupil modal and compares it with
% the data ellipse added as an input in the algebraic form and returns error
% between the five points chosen on the two ellipses.

[A2,B2,C2,c2] = calculate_model_matrix(alpha, fz, a_EC, b_EC, pitch, roll, yaw, r);

error = error_between_two_ellipses(A1,B1,C1,c1,A2,B2,C2,c2);