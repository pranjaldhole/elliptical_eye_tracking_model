function [alpha_c, fz_c, a_EC_c, b_EC_c, fval] = optimize_global(A,B,C,ellipsecent, maxiter, species)
%[alpha_c, fz_c, a_EC_c, b_EC_c, fval] = optimize_global(A,B,C,ellipsecent, maxiter, species)
%
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the marked pupil ellipses as in input and
% computes the values of global parameters of the pupil by doing fminsearch
% over the global variables. Input 'species' takes a string as input
% that specifies the species of animal and makes accordingly the choice of
% model to fit the data. So far, the function
% uses either elliptical or circular pupil model to fit to the input
% species.
% The subfunction q2p reparameterizes the global parameters further. This has
% been done in order to avoid infinities and NaN values during fminsearch.
% The subfunction ofuncfromp is the objective function for the fminsearch over
% p parameters. The reparameterization p2q is done to recover
% back the global parameters.

if ~exist('maxiter', 'var') || isempty(maxiter), maxiter = 500; end

%if ~exist('species', 'var') || isempty(species), species = 'rat'; end

[start_a_EC, start_b_EC] = initialize_eyeball_center(ellipsecent);
min_fz_initial = calculate_min_fz(ellipsecent, start_a_EC, start_b_EC);

switch lower(species)
    case {'rat', 'mouse'} %fix alpha = 1
        
        start_point_global = [1, 1.25 * min_fz_initial, start_a_EC, start_b_EC];
        p0 = q2p(start_point_global, ellipsecent);
        p0 = p0(2:end);
        objective_function = @(p) ofuncfromp([inf p], A,B,C,ellipsecent);
        
    case 'tree shrew' %alpha is free
        
        start_point_global = [0.9, 1.25 * min_fz_initial, start_a_EC, start_b_EC];
        p0 = q2p(start_point_global, ellipsecent);
        objective_function = @(p) ofuncfromp(p, A,B,C,ellipsecent);
        
    case {'cat', 'ferret'}
        
        error('%s eye tracking is not yet implemented', species);
        
    case ''
        
        error('species must be specified prior to calibration');
        
    otherwise
        
        error('unrecognized species: %s', species);
        
end
wb = waitbar(0,'Calibrating camera under weak perspective model','windowstyle','modal');
[p,fval, ~, ~] = fminsearch(objective_function, p0, ...
        optimset('tolx', 1e-3, 'tolfun', 0.001, 'maxiter', maxiter, 'maxfunevals', 1000));
waitbar(1,wb);

switch lower(species)
    case {'mouse' 'rat'}
        
        p = [inf p];
        
    case 'tree shrew'
        
        %do nothing
        
    case {'cat', 'ferret'}
        
        error('%s eye tracking is not yet implemented', species);
        
    otherwise
        
        error('unrecognized species: %s', species);
        
end

q = p2q(p, ellipsecent);
close(wb);
[alpha_c, fz_c, a_EC_c, b_EC_c] = deal(q(1), q(2), q(3), q(4));


function e = ofuncfromp(p, A,B,C,ellipsecent)

q = p2q(p, ellipsecent);
e = calculate_mean_sum_square_errors(A,B,C,ellipsecent, q(1), q(2), q(3), q(4));


function q = p2q(p, ellipsecent)

min_fz_initial = calculate_min_fz(ellipsecent, p(3), p(4));

q(1) = 1 / (1 + exp(-p(1)));
q(2) = min_fz_initial * (1 + exp(p(2)));
q(3) = p(3);
q(4) = p(4);


function p = q2p(q, ellipsecent)

min_fz_initial = calculate_min_fz(ellipsecent, q(3), q(4));

p(1) = log( q(1) / (1 - q(1)) );
p(2) = log( ( q(2) / min_fz_initial ) - 1);
p(3) = q(3);
p(4) = q(4);