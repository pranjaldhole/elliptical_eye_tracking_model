function graphical_comparison_between_circular_and_elliptical_model()
% graphical_comparison_between_circular_and_elliptical_model()
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This script imports the 'treeshrew_rat_comparison_R_eye_027_v3.eyd.mat' file which
% contains values of squared errors for circular pupil and elliptical pupil
% model and plots them in the graphical format for the comparison.

load('C:\Users\dhole\Documents\eet\treeshrew_rat_comparison_R_eye_027_v3.eyd.mat','-mat');

figure;
hold on;
plot(squared_errors_rat, squared_errors_treeshrew, 'x');
title('Comparison between elliptical pupil model and circular pupil model');
axis equal;
xlabel('Sum of Squared error in circular model');
ylabel('Sum of Squared error in elliptical model');

[ssqerat_sorted, si] = sort(squared_errors_rat);
ssqetw_sorted = squared_errors_treeshrew(si);

n = (1:numel(squared_errors_rat))';
ii = max(1, numel(squared_errors_rat) - 4):numel(squared_errors_rat);
text(ssqerat_sorted(ii), ssqetw_sorted(ii), num2str(n(si(ii))));

plot([0; (max(squared_errors_rat) + 10)],[0; (max(squared_errors_rat) + 10)],'-');