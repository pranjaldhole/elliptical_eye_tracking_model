function generate_simulated_data(Loops, alpha, fz, a_EC, b_EC)
% generate_simulated_data(Loops, alpha, fz, a_EC, b_EC)
% 
%%%%%%%%% Description %%%%%%%%%%%%%
% This program takes the values of global parameters as inputs and
% generates 'Loops' number of randomly generated ellipses in the matrix
% form and saves them along with values of local and global parametes 
% into '-mat' formt.
%%%%%%% Description of data in arrays %%%%%%%%%%%%%%%
% Tloc = [Tloc; 'r' 'pitch' 'roll' 'yaw'];
% Trot = [Trot; 'r' 'R11' 'R12' 'R13' 'R21' 'R22' 'R23' 'R31' 'R32' 'R33'];
% Tpoints = [Tpoints; 'x1','x2','x3','x4','x5','x6','x7','y1','y2','y3','y4','y5','y6','y7'];
% Tdata = [Tdata; 'd11','d12','d13','d22','d23','d33'];
% Loops: insert how many trials of data you want to generate.

%%%%%%% Creating tables to append the values %%%%%%%%
Tloc = zeros(0,4);
% Trot = zeros(0,10);
Tpoints = zeros(0,14);
Tdata = zeros(0,6);


%%%%%% Generating 'Loops' number of trials of simulated data %%%%%%%%%%%%
i = 1;
while i <= Loops
    
    %%% Generating random values of local parameters in the image %%%%%%%%%
    rmax = fz;
    rmin = 0.3 * fz;
    r = rmin + rand(1) * (rmax - rmin);% This makes sure r is never higher than fz.
    xmax = pi / 4;
    xmin = - pi / 4;
    yaw = xmin + rand(1) * (xmax - xmin);    % yaw % These values are in radians.
    pitch = xmin + rand(1) * (xmax - xmin);  % pitch % rand selects values between (0,1) randomly.
    roll = xmin + rand(1) * (xmax - xmin);    % roll %
    
    %%%%%%%%%% Calculation of matrices %%%%%%%%%%%%%%%%
    
    data_matrix = calculate_model_matrix(alpha, fz, a_EC, b_EC, pitch, roll, yaw, r);
    
    %%%%%%%%%%% Calculating the data points %%%%%%%%%%%%%%%%%%%%%
    [A,B,C,c] = mat2alg(data_matrix);
    [L,l,omega] = alg2trig(A, B, C);
    
    d11 = data_matrix(1,1);
    d12 = data_matrix(1,2);
    d13 = data_matrix(1,3);
    d22 = data_matrix(2,2);
    d23 = data_matrix(2,3);
    d33 = data_matrix(3,3);
    
    centerx = c(1); %% Center point of the ellipse %%
    centery = c(2); %% Center point of the ellipse %%
    %Parametric coordinates of ellipse
    % x = centerx + major_axis * cos(inclination)
    % y = centery + minor_axis * sin(inclination)
    x1 = centerx + L * cos(omega + 0);
    y1 = centery + l * sin(omega + 0);
    
    x2 = centerx + L * cos(omega + pi / 4);
    y2 = centery + l * sin(omega + pi / 4);
    
    x3 = centerx + L * cos(omega + pi / 2);
    y3 = centery + l * sin(omega + pi / 2);
    
    x4 = centerx + L * cos(omega + 0.75 * pi);
    y4 = centery + l * sin(omega + 0.75 * pi);
    
    x5 = centerx + L * cos(omega + pi / 3);
    y5 = centery + l * sin(omega + pi / 3);
    
    x6 = centerx + L * cos(omega + pi / 6);
    y6 = centery + l * sin(omega + pi / 6);
    
    x7 = centerx + L * cos(omega + 5 * pi / 6);
    y7 = centery + l * sin(omega + 5 * pi / 6);
    
    %%%%%%%%%%% Appending the data to matrices %%%%%%%%%%%%%%%%%%%%
    
    Tloc = [Tloc; pitch roll yaw r];
%     Trot = [Trot; r R(1,1) R(1,2) R(1,3) R(2,1) R(2,2) R(2,3) R(3,1) R(3,2) R(3,3)];
    Tpoints = [Tpoints; x1,x2,x3,x4,x5,x6,x7,y1,y2,y3,y4,y5,y6,y7];
    Tdata = [Tdata; d11,d12,d13,d22,d23,d33];
    
    i = i + 1;
end;

%%%%%%%%%%% Importing the data to file %%%%%%%%%%%%%%%%%%%%%%
filename = ['simdata_' num2str(Loops) 'trials.mat'];
save(filename,'alpha','fz','a_EC','b_EC','Tdata','Tloc','Tpoints');