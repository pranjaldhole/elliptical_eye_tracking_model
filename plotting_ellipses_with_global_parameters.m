function plotting_ellipses_with_global_parameters(Tdata, model_matrices, alpha, fz, a_EC, b_EC, linewidth, species) % species
%plotting_ellipses_with_global_parameters(Tdata, model_matrices, alpha, fz, a_EC, b_EC, linewidth, species) % species
%
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the data and the information about the model matrices
% for cicular/elliptical model as input along with the values of global
% parameters and plots the data ellipses with its fits from the model and
% global parameters.

% drawing the ellipse
ndrawpts = 1000;
theta_r         = linspace(0, 2 * pi, ndrawpts);
xypts = [cos(theta_r); sin(theta_r)];

figure;
hold on;

for i = 1:size(Tdata,1)
    new_data_matrix = Tdata(i,:);
    data_matrix = [new_data_matrix(1) new_data_matrix(2) new_data_matrix(3); new_data_matrix(2) new_data_matrix(4) new_data_matrix(5); new_data_matrix(3) new_data_matrix(5) new_data_matrix(6)];
    
    new_model_matrix = model_matrices(i,:);
    model_matrix = [new_model_matrix(1) new_model_matrix(2) new_model_matrix(3); new_model_matrix(2) new_model_matrix(4) new_model_matrix(5); new_model_matrix(3) new_model_matrix(5) new_model_matrix(6)];
    

    plot_two_ellipses_matrix_form(data_matrix, model_matrix, linewidth);
end

plot(a_EC, b_EC, 'm*');
% Plotting Fz:
circle_scaled = diag([fz fz]) * xypts;
fz_circle = [circle_scaled(1,:) + a_EC; circle_scaled(2,:) + b_EC];
plot(fz_circle(1,:), fz_circle(2,:), 'k--');

% Shape of ellipse
r = 150;
omega = 0;

pupil_scaled = diag([r alpha*r]); %mapping a circle to ellipse
Rot = [cos(omega) -sin(omega); sin(omega) cos(omega)]; %2 dimensional rotation matrix to define inclination of ellipse
ellipse_r = Rot * pupil_scaled * xypts + [a_EC; b_EC] * ones(1, ndrawpts);
plot(ellipse_r(1,:), ellipse_r(2,:)), 'b--';

hold off;
title( sprintf('%s model', species));
axis equal;