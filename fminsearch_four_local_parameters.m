function [pitch, roll, yaw, r, fv] = fminsearch_four_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1)
% [pitch, roll, yaw, r] = fminsearch_four_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
%  This function uses the global parameters and the ellipses in algebraic
%  form obtained from the marked pupil data as inputs and performs fminsearch over all the four
%  local variables and returns their optimal values as outputs.

[pitch, roll, yaw, r] = fminsearch_two_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1);
p0 = [pitch roll yaw r];
assert(~any(isnan(p0)) && all(imag(p0) == 0) && ~any(isinf(p0)), 'invalid 3D pose');

objective_function = @(p) error_between_model_and_data(alpha, fz, a_EC, b_EC, p(1), p(2), p(3), p(4), A1,B1,C1,c1);

[p, fv,~,~] = fminsearch(objective_function, p0, optimset('tolx', 1e-3, 'tolfun', 0.1, 'maxiter', 10000, 'maxfunevals', 10000));

assert(~any(isnan(p)) && all(imag(p) == 0) && ~any(isinf(p)), 'invalid 3D pose');

pitch = p(1);
roll = p(2);
yaw = p(3);
r = p(4);