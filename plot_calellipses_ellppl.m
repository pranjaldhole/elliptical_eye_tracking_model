function plot_calellipses_ellppl(ellipsecent, theta, majax, minax, cproj, theta_proj, majax_proj, minax_proj, species)
%plotting_ellipses_with_global_parameters(Tdata, model_matrices, alpha, fz, a_EC, b_EC, linewidth, species)
%
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the marked pupil data and the fits to the data from the model
% as input and plots the data ellipses with its fits from the model and
% global parameters.
global eyedata
if ~exist('linewidth', 'var') || isempty(linewidth), linewidth = 1; end
n = numel(majax);
ndrawpts = 1000;
theta_r         = linspace(0, 2 * pi, ndrawpts);
xypts = [cos(theta_r); sin(theta_r)];

figure;
hold on;

for i = 1:n
    [h1, h2, h3, h4] = plot_two_ellipses(majax(i), minax(i), theta(i), ellipsecent(:,i)', majax_proj(i), minax_proj(i), theta_proj(i), cproj(i,:), linewidth);
end

j1 = plot(eyedata.weakp.ab(1), eyedata.weakp.ab(2), 'm*');
% legend('Eyeball center');
% Plotting Fz:
circle_scaled = diag([eyedata.weakp.fzrat eyedata.weakp.fzrat]) * xypts;
fz_circle = [circle_scaled(1,:) + eyedata.weakp.ab(1); circle_scaled(2,:) + eyedata.weakp.ab(2)];
j2 = plot(fz_circle(1,:), fz_circle(2,:), 'k--');
% legend('f/z0 ratio');

% Shape of ellipse
r = 150;
omega = 0;

pupil_scaled = diag([r eyedata.weakp.alpha * r]); %mapping a circle to ellipse
Rot = [cos(omega) -sin(omega); sin(omega) cos(omega)]; %2 dimensional rotation matrix to define inclination of ellipse
ellipse_r = Rot * pupil_scaled * xypts + [eyedata.weakp.ab(1); eyedata.weakp.ab(2)] * ones(1, ndrawpts);
j3 = plot(ellipse_r(1,:), ellipse_r(2,:), 'b--');
% legend('Fitted ellipse along the optical axis');
hold off;
title( sprintf('%s model', species));
legend([h1 h2 h3 h4 j1 j2 j3], {'Marked ellipse center','Fitted ellipse center','Marked ellipse', 'Fitted ellipse',...
    'Eyeball center', 'f/z0 ratio', 'Modelled pupil along the optical axis'});
axis equal;