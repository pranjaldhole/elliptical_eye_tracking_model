function [alpha, min_fz, a_EC, b_EC] = initialize_global_variables(Tdata)
% [alpha, min_fz, a_EC, b_EC] = initialize_global_variables(Tdata)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the data ellipses as an input and returns the value
% of eyeball center whcih is computed as a mean of all the ellipse centers
% in the data as in initialization value.
alpha = 0.9;
n = size(Tdata,1);
a_PCs = zeros(1,n);
b_PCs = zeros(1,n);
pupil_rotation_radii = zeros(1,n);
for i = 1:n
    data_matrix = [Tdata(i,1) Tdata(i,2) Tdata(i,3); Tdata(i,2) Tdata(i,4) Tdata(i,5); Tdata(i,3) Tdata(i,5) Tdata(i,6)];
    [~,~,~,c] = mat2alg(data_matrix);
    a_PCs(i) = c(1);
    b_PCs(i) = c(2);
end
a_EC = mean(a_PCs);
b_EC = mean(b_PCs);

for i = 1:n
    pupil_rotation_radii(i) = sqrt( (a_EC - a_PCs(i)) ^ 2 + (b_EC - b_PCs(i)) ^ 2);
end
min_fz = 1.25 * max(pupil_rotation_radii);