# Extended 3D model for pupil-tracking

In this project, we build a model using weak perspective to track elliptical pupils.

We collect several images of the pupil in a single session on which the pupil boundary is marked. For the optimization to work best, we need at least 10 or more images to process. 

This eye-data is extracted using eydimportscript.m script into matrix format that contains information about the ellipses in the trigonometric format, viz. pupil center x, pupil center y, inclination in radians, major axis, minor axis in columns. 

This data is then converted to another matrix format using load_Tdata.m. The matrix Tdata is an nX6 matrix in which each row contains the elements of the symmetric conic section matrix in order: [(1,1) (1,2) (1,3) (2,2) (2,3) (3,3)] which is our data ellipse for each image. 

We use Tdata as an input to optimize_global.m which uses Nelder-Mead algorithm using the fminsearch function of matlab to minimize the mean sum of the square error between 5 points between the ellipse from the data and the elliptical pupil model. 

The function optimize_global.m evaluates the values of the physical parameters of the pupil (termed as global parameters), viz., ratio of minor to the major axis (alpha), maximum pupil rotation radius in pixels, and the eyeball center.

The evaluated global parameters of the pupil can then be used to calibrate the eye so that we can mark the boundary of the pupil for the image data using the eye-tracking software developed at BBO lab, CAESAR, Bonn, Germany.