function [start_a_EC, start_b_EC] = initialize_eyeball_center(ellipsecent)
% [start_a_EC, start_b_EC] = initialize_eyeball_center(ellipsecent)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the marked pupil ellipse centres as an input and returns the value
% of eyeball center which is computed as a mean of all the ellipse centers
% in the marked pupil data as in initialization value.
n = numel(ellipsecent) / 2;

start_a_EC = mean(ellipsecent(1,:));
start_b_EC = mean(ellipsecent(2,:));