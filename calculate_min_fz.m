function min_fz = calculate_min_fz(ellipsecent, a_EC, b_EC)
%  min_fz = calculate_min_fz(ellipsecent, a_EC, b_EC)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes the eyeball center and the marked pupil ellipse
% centres and calculate the minimum value of fz which is determined by 
% taking maximum value of pupil rotation radius computed as distance 
% between the center of ellipse from the input data ellipses and the 
% eyeball center.
n = numel(ellipsecent)/2;
pupil_rotation_radii = zeros(0,n);
for i = 1:n
    pupil_rotation_radii(i) = sqrt( (a_EC - ellipsecent(1,i)) ^ 2 + (b_EC - ellipsecent(2,i)) ^ 2);
end
min_fz = max(pupil_rotation_radii);