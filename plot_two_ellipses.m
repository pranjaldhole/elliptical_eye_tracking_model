function [h1 h2 h3 h4] = plot_two_ellipses(major_axis1, minor_axis1, orientation1, center1, major_axis2, minor_axis2, orientation2, center2, linewidth)
% Caution: Comment lines 35, 37, 42 to use the function in a loop statement
% if one needs to write several plots in one figure.
% 
%plot_two_ellipses(major_axis1, minor_axis1, orientation1, center1, major_axis2, minor_axis2, orientation2, center2, linewidth)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function take two ellipses in the trigonometric form and plots them into
% the same graph.

%%%%% ellipse parameters:
[A1,B1,C1] = trig2alg(major_axis1, minor_axis1, orientation1);

[A2,B2,C2] = trig2alg(major_axis2, minor_axis2, orientation2);

[~,p1,p2] = ellipse_efunc(A1,B1,C1,center1,A2,B2,C2,center2); % points on two ellipses

R1 = [ cos(orientation1) -sin(orientation1); sin(orientation1) cos(orientation1) ];
R2 = [ cos(orientation2) -sin(orientation2); sin(orientation2) cos(orientation2) ];

S1 = diag([major_axis1 minor_axis1]);
S2 = diag([major_axis2 minor_axis2]);

% drawing the ellipse
ndrawpts = 1000;
theta_r         = linspace(0, 2 * pi, ndrawpts);
xypts = [cos(theta_r); sin(theta_r)];

ellipsepts1 = R1 * S1 * xypts + center1(:) * ones(1, ndrawpts);
ellipsepts2 = R2 * S2 * xypts + center2(:) * ones(1, ndrawpts);

% figure;
h1 = plot(p1(:,1), p1(:,2), 'r*');
% hold on;
h2 = plot(p2(:,1), p2(:,2), 'g*');
h3 = plot( ellipsepts1(1,:), ellipsepts1(2,:),'r','linewidth',linewidth );
h4 = plot( ellipsepts2(1,:), ellipsepts2(2,:),'g','linewidth',linewidth );
axis equal;
legend([h1 h2 h3 h4],{'Marked ellipse center','Fitted ellipse center','Marked ellipse', 'Fitted ellipse'});
% hold off;