%%%%%%%%%%%%% Description %%%%%%%%%%%%
% We use this script to convert calibration data from .eyd format to raw
% data for the marked pupil in trigonometric form. The usable output of the
% script is matrix called pupildata. The column order for the matrix is 
% explained above in the script. 

xx = load('/data/elliptical_eye_tracking_data/R_eye_027_v3.eyd','-mat');

cframes = xx.eyedata.weakp.calibframes;

pupildata_raw = xx.eyedata.vid.ellipse(cframes, 1:5);
%column order:
%pupil center x, pupil center y, inclination in radians, major axis, minor axis

cornerdata = xx.eyedata.vid.ellipse_eye(cframes, 6:9);

if xx.eyedata.params.ncorners > 1
    
    cornerx = cornerdata(:, 1);
    cornery = cornerdata(:, 2);
    
else

    cornerx = mean_nonnan(cornerdata(:, [1 3]));
    cornery = mean_nonnan(cornerdata(:, [2 4]));
    
end

pupildata = pupildata_raw;
pupildata(:, 1) = pupildata(:, 1) - cornerx;
pupildata(:, 2) = pupildata(:, 2) - cornery;

ellipsecent = [pupildata(:,1) pupildata(:,2)]';
theta = pupildata(:,3);
majax = pupildata(:,4);
minax = pupildata(:,5);

clear xx;