function plotting_estimated_parameters_with_ellipses(Tdata, model_matrices, species)
%plotting_estimated_parameters_with_ellipses(Tdata, model_matrices, alpha, fz, a_EC, b_EC)
%
%%%%%%%%%%%%% Description %%%%%%%%%%%%
% This is the short version of  plotting_ellipses_with_global_parameters
% function that only plots the data ellipses along with the ellipses
% computed from the model.

load('C:\Users\dhole\Documents\eet\treeshrew_rat_comparison.mat','-mat');

figure;
hold on;
for i = 34%1:size(Tdata,1)
    new_data_matrix = Tdata(i,:);
    data_matrix = [new_data_matrix(1) new_data_matrix(2) new_data_matrix(3); new_data_matrix(2) new_data_matrix(4) new_data_matrix(5); new_data_matrix(3) new_data_matrix(5) new_data_matrix(6)];
    
    new_model_matrix = model_matrices(i,:);
    model_matrix = [new_model_matrix(1) new_model_matrix(2) new_model_matrix(3); new_model_matrix(2) new_model_matrix(4) new_model_matrix(5); new_model_matrix(3) new_model_matrix(5) new_model_matrix(6)];
    
    plot_two_ellipses_matrix_form(data_matrix, model_matrix);
end
hold off;
title(sprintf('%s model', species)');
axis equal;
