function [pitch, roll, yaw, r] = fminsearch_two_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1)
% [pitch, roll, yaw, r] = fminsearch_two_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1)
% 
%%%%%%%%%%%%% Description %%%%%%%%%%%%
%  This function uses the global parameters and the ellipses in algebraic from
%  obtained from the marked pupil data as an input. After initializing the local 
% parameters using the weak perspective model, it performs fminsearch over two of the four
%  local variables and returns the four local parameters as outputs.

[pitch, roll, yaw, r] = initialize_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1);
p0 = [roll, r];
objective_function = @(p) error_between_model_and_data(alpha, fz, a_EC, b_EC, pitch, p(1), yaw, p(2), A1,B1,C1,c1);
[p, ~,~,~] = fminsearch(objective_function, p0, optimset('tolx', 1e-3, 'tolfun', 0.1, 'maxiter', 10000, 'maxfunevals', 10000));
roll = p(1);
r = p(2);