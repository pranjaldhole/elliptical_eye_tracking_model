function [pitch, roll, yaw, r] = initialize_local_parameters(~, fz, a_EC, b_EC, A1,B1,C1,c1)
% [pitch, roll, yaw, r] = initialize_local_parameters(alpha, fz, a_EC, b_EC, A1,B1,C1,c1)
% 
% %%%%%%%%%%%%% Description %%%%%%%%%%%%
% This function takes global parameters and marked pupil ellipse 
% in the algebraic format and computes the values for the local
% parameters that can be used as an initialization during optimization
% using fminsearch.

[major_axis] = alg2trig(A1, B1, C1);

%%% Pupil center:
a_PC = c1(1); %% Pupil center(x) %%
b_PC = c1(2); %% Pupil center(y) %%

%%% Values for local parameters to start initialization:
pitch = asin(-(a_EC - a_PC) / fz);
if imag(pitch) ~= 0
    keyboard
end
yaw = -asin((b_EC - b_PC) / fz / cos(pitch));
if imag(yaw) ~= 0
    keyboard
end
r = major_axis / fz;
% Random starting point value for torsion:
roll = 0.5;